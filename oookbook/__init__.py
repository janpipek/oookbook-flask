import os

from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from flask_migrate import Migrate

from config import config

template_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
static_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')

database = SQLAlchemy()
bootstrap = Bootstrap()
migrate = Migrate()

from . import models

login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.anonymous_user = models.AnonymousUser

config_name = os.environ.get("ENVIRONMENT", "default")

app = Flask(__name__, template_folder=template_dir, static_folder=static_dir)
app.config.from_object(config[config_name])

database.init_app(app)
bootstrap.init_app(app)
login_manager.init_app(app)
migrate.init_app(app, database)

from .auth import auth as auth_blueprint
app.register_blueprint(auth_blueprint, url_prefix="/auth/")

from .main import main as main_blueprint
app.register_blueprint(main_blueprint)
