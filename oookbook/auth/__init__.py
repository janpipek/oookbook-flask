from flask import Blueprint, current_app, abort
from flask_login import current_user
from functools import wraps

auth = Blueprint('auth', __name__)

from . import views


def right_required(*rights):
    """Decorator that requires right to be present for the user role.

    If user does not have the right:
    * not logged in => redirect to login page
    * logged in => forbidden (code 403)

    In most cases, it contains login_required behaviour as well.
    """
    def right_decorator(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            for right in rights:
                if not current_user.can(right):
                    if not current_user.is_authenticated():
                        return current_app.login_manager.unauthorized()
                    else:
                        abort(403)
            return func(*args, **kwargs)
        return decorated_view
    return right_decorator