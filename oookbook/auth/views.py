from flask import render_template, redirect, url_for, flash, request
from flask_login import login_user, logout_user, login_required

from . import auth
from .forms import LoginForm
from ..models import User
from .. import login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@auth.route("login", methods=['get', 'post'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.check_password(form.password.data):
            login_user(user)
            flash("Welcome %s" % user.username)
            return redirect(request.args.get('next') or url_for('main.index'))
        flash("Invalid user name or password")

    return render_template("auth/login.html", form=form)


@auth.route("logout")
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main.index'))
