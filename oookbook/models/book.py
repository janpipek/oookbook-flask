from .. import database

from sqlalchemy import Column, ForeignKey, func
from sqlalchemy import Integer, Unicode, String, TIMESTAMP, UnicodeText

import isbnlib
import iso639

class Book(database.Model):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(250), nullable=False)
    authors = Column(Unicode(250))
    owner_id = Column(Integer, ForeignKey("users.id"))
    publisher = Column(Unicode(150))
    year = Column(Unicode(30))
    isbn = Column(String(20))                          # ISBN-13
    _language = Column(String(20), name="language")    # ISO 639-2
    inserted_at = Column(TIMESTAMP, index=True, server_default=func.current_timestamp())
    description = Column(UnicodeText)                  # Public description
    private_note = Column(UnicodeText)                 # Private description

    @property
    def language_name(self):
        """English name of the language"""
        try:
            return iso639.to_name(self.language)
        except:
            return None

    @property
    def language(self):
        """ISO 639-2 code of the language."""
        return self._language

    @language.setter
    def language(self, value):
        if not value:
            self._language = None
        else:
            self._language = iso639.to_iso639_2(value)

    def __repr__(self):
        return '<Book %r>' % self.title

    @staticmethod
    def create_from_isbn(isbn):
        if not isbn:
            return None
        try:
            data = isbnlib.meta(str(isbn), "goob") # Default sometimes gives wrong results
            book = Book()
            book.title = data["Title"]
            book.authors = ";".join(data["Authors"])
            book.isbn = data["ISBN-13"]
            book.publisher = data["Publisher"]
            book.year = data["Year"]
            book.language = data["Language"]
            return book
        except:
            return None

    @staticmethod
    def create_from_dict(a_dict):
        if not a_dict:
            raise "Must be a non-empty dictionary to convert to book."
        book = Book()
        book.title = a_dict.get("title", "")
        book.authors = a_dict.get("authors", "")
        book.year = a_dict.get("year", "")
        book.isbn = a_dict.get("isbn", "")
        book.publisher = a_dict.get("publisher", "")
        book.language = a_dict.get("language", "")
        book.description = a_dict.get("description", "")
        book.private_note = a_dict.get("private_note", "")
        return book