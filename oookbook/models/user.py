from flask_login import UserMixin, AnonymousUserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import Column, func
from sqlalchemy import Integer, Unicode, String, TIMESTAMP

from .. import database
from .roles import get_role

class UserAuthMixin(object):
    @property
    def role_display_name(self):
        return get_role(self.role).display_name

    @property
    def rights(self):
        """A list of user right names."""
        return get_role(self.role).rights

    def can(self, right):
        return right in self.rights


class User(UserMixin, UserAuthMixin, database.Model):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String(64), unique=True, nullable=False)
    password_hash = Column(String(100), nullable=False)
    email = Column(Unicode(128), unique=True, nullable=False)
    inserted_at = Column(TIMESTAMP, index=True, server_default=func.current_timestamp())
    role = Column(String(64), default="user", nullable=False)

    books = database.relationship('Book', backref='owner')

    def __init__(self, username, email):
        self.username = username
        self.email = email

    def __repr__(self):
        return '<User %r>' % self.username

    @property
    def password(self):
        raise AttributeError("Password is not a readable property.")

    @password.setter
    def password(self, value):
        """Setter for password that automatically hashes it."""
        self.password_hash = generate_password_hash(value)

    def check_password(self, password):
        """Check whether supplied password is valid."""
        return check_password_hash(self.password_hash, password)

    @property
    def book_count(self):
        return len(self.books)


class AnonymousUser(AnonymousUserMixin, UserAuthMixin):
    @property
    def username(self):
        return "anonymous"

    @property
    def role(self):
        return "anonymous"