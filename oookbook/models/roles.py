import json
import os


class Role(object):
    """A simple role object that aggregates rights.

    It can inherit rights from a parent in a tree-like structure.
    It is not stored in the database.
    """
    def __init__(self, name, parent=None, rights=[]):
        self.name = name
        self.parent = parent
        self._rights = list(rights)

    @property
    def rights(self):
        if self.parent:
            return get_role(self.parent).rights + self._rights
        else:
            return self._rights


def get_role(name):
    return _data.get(name)

# Initial reading of roles from JSON file.
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'roles.json')) as f:
    _raw_data = json.load(f)
    _data = {}
    for key, value in _raw_data.items():
        parent = value.get("inherit")
        rights = value.get("rights")
        _data[key] = Role(key, parent, rights)