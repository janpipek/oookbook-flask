from flask.views import MethodView
from flask import render_template, abort, redirect, url_for, flash, request, current_app
from flask_login import login_required, current_user
from sqlalchemy import desc
import json
import os

from .. import app
from ..models import Book, User
from . import main
from .forms import AddBookForm, EditBookForm, IsbnAddForm, JsonUploadForm, ChangePasswordForm
from .. import database
from ..auth import right_required


@main.route("/")
def index():
    return render_template("index.html")


class BookListView(MethodView):
    decorators = [right_required("show_book")]

    def get(self):
        page = request.args.get('page', 1, type=int)
        pagination = Book.query.order_by(desc(Book.id)).paginate(page, per_page=30)
        books = pagination.items

        return render_template("books/list.html",
                               books=books, pagination=pagination)


class BookShowView(MethodView):
    decorators = [right_required("show_book")]

    def get(self, book_id):
        book = Book.query.filter_by(id=book_id).first()
        enable_edit = (current_user.can("edit_book")
                       or current_user.can("edit_own_book") and book.owner == current_user)
        if not book:
            abort(404)
        return render_template("books/show.html", book=book, enable_edit=enable_edit)


class BookAddView(MethodView):
    decorators = [right_required("add_book")]

    def get(self):
        book_form = AddBookForm()
        return render_template("books/add.html", book_form=book_form)

    def post(self):
        book_form = AddBookForm()
        if book_form.validate():
            book = Book()
            book.title = book_form.title.data
            book.authors = book_form.authors.data
            book.publisher = book_form.publisher.data
            book.isbn = book_form.isbn.data
            book.year = book_form.year.data
            book.language = book_form.language.data

            book.owner = current_user
            database.session.add(book)
            database.session.commit()
            flash("New book successfully added.")
            return redirect(url_for(".my_library"))
        else:
            abort(501)


@main.route("/books/add_by_isbn", methods=('post',))
@right_required("add_book")
def book_add_by_isbn():
    isbn_form = IsbnAddForm()
    if isbn_form.validate_on_submit():
        isbn = isbn_form.isbn.data
        book = Book.create_from_isbn(isbn)
        if book:
            book_form = AddBookForm(formdata=None)
            book_form.apply_book(book)
            flash("Book data automatically filled from Google Books. Please, revise the information.")
        else:
            book_form = AddBookForm()
            if isbn:
                flash("Book with the selected ISBN was not found.")
        return render_template("books/add.html", book_form=book_form)
    else:
        abort(501)


@main.route("/books/copy")
@right_required("add_book", "show_book")
def book_copy():
    book_id = request.args.get('book_id', 0, type=int)
    book = Book.query.filter_by(id=book_id).first()
    if not book:
        abort(404)
    book_form = AddBookForm(formdata=None)
    book_form.apply_book(book, book.owner == current_user)
    return render_template("books/add.html", book_form=book_form)


@main.route("/books/<int:book_id>/edit", methods=('get', 'post'))
@login_required
def book_edit(book_id):
    book = Book.query.filter_by(id=book_id).first()
    if not book:
        abort(404)
    if not (current_user.can("edit_book")
             or current_user.can("edit_own_book")
             and book.owner == current_user):
        abort(403)

    book_form = EditBookForm()
    if book_form.validate_on_submit():
        book.title = book_form.title.data
        book.authors = book_form.authors.data
        book.publisher = book_form.publisher.data
        book.isbn = book_form.isbn.data
        book.year = book_form.year.data
        book.language = book_form.language.data
        book.description = book_form.description.data
        if book.owner == current_user:
            book.private_note = book_form.private_note.data
        database.session.add(book)
        database.session.commit()
        flash("Book successfully updated.")
        return redirect(url_for(".book_show", book_id=book.id))
    else:
        book_form.apply_book(book, book.owner == current_user)
        return render_template("books/edit.html", book_form=book_form)


class UserListView(MethodView):
    decorators = [right_required("show_user")]

    def get(self):
        users = User.query.all()
        return render_template("users/list.html", users=users)


class UserShowView(MethodView):
    decorators = [right_required("show_user")]

    def get(self, username):
        user = User.query.filter_by(username=username).first()
        if not user:
            abort(404)
        return render_template("users/show.html", user=user)


@main.route("/library/")
@login_required
def my_library():
    page = request.args.get('page', 1, type=int)
    #pagination = current_user.books.paginate(page=page,
    #                                    per_page=current_app.config.get('PER_PAGE'))
    books = current_user.books #agination.items
    return render_template("library/index.html", books=books,
                           isbn_form = IsbnAddForm())
    # return render_template("library/index.html", books=books, pagination=pagination)


@main.route("/library/export")
@login_required
def library_export():
    '''A complete JSON dump of user's database.'''
    books = current_user.books
    jsonifiable = [
        {
            "title" : book.title,
            "authors" : book.authors,
            "publisher" : book.publisher,
            "language" : book.language,
            "year" : book.year,
            "isbn" : book.isbn
        }
        for book in books
    ]
    return json.dumps(jsonifiable)


@main.route("/library/import")
@login_required
def library_import():
    json_form = JsonUploadForm()
    return render_template("library/import.html", json_form=json_form)


def _import_file_path():
    '''Where the uploaded library JSON file is stored.'''
    file_name = "import-%s.json" % current_user.username
    upload_dir = app.config['UPLOAD_FOLDER']
    return os.path.join(upload_dir, file_name)


@main.route("/library/import/review", methods=('post',))
@login_required
def library_import_review():
    json_form = JsonUploadForm()
    if json_form.validate_on_submit():
        file_path = _import_file_path()
        json_form.json_file.data.save(file_path)
        with open(file_path, "r") as f:
            file_content = f.read()
        data = json.loads(file_content)
        books = [Book.create_from_dict(x) for x in data]
        return render_template("library/import_review.html", books=books)
    else:
        flash("Invalid form data.")
        return render_template("library/import.html", json_form=json_form)


@main.route("/library/import/finalize", methods=('post', ))
@login_required
def library_import_finalize():
    file_path = _import_file_path()
    with open(file_path, "r") as f:
        file_content = f.read()
    data = json.loads(file_content)
    books = [Book.create_from_dict(x) for x in data]
    for book in books:
        book.owner = current_user
        database.session.add(book)
    database.session.commit()
    os.remove(file_path)   # TODO: A clean up task has to be present as well.
    flash("Books successfully imported.")
    return redirect(url_for(".my_library"))


@main.route("/profile/edit", methods=('get', 'post'))
@login_required
def edit_profile():
    password_form = ChangePasswordForm()
    if password_form.validate_on_submit():
        if not current_user.check_password(password_form.original.data):
            password_form.original.errors.append("Current password not valid.")
        else:
            current_user.password = password_form.new.data
            database.session.add(current_user)
            database.session.commit()
            flash("Password changed.")
            return redirect(url_for(".user_show", username=current_user.username))
    return render_template("profile/edit.html", password_form=password_form)


main.add_url_rule("/books/", view_func=BookListView.as_view("book_list"))
main.add_url_rule("/books/<int:book_id>/", view_func=BookShowView.as_view("book_show"))
main.add_url_rule("/books/add", view_func=BookAddView.as_view("book_add"))

main.add_url_rule("/users/", view_func=UserListView.as_view("user_list"))
main.add_url_rule("/users/<username>", view_func=UserShowView.as_view("user_show"))