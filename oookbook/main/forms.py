from flask_wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, PasswordField
from wtforms.validators import DataRequired, EqualTo
from flask_wtf.file import FileField, FileAllowed, FileRequired


class BookForm(Form):
    title = StringField("Book title")
    authors = StringField("List of authors")
    publisher = StringField("Publisher")
    year = StringField("Year")
    isbn = StringField("ISBN")
    language = StringField("Language")
    description = TextAreaField("Description (public)")
    private_note = TextAreaField("Private note")

    def apply_book(self, book, own=False):
        self.title.data = book.title
        self.authors.data = book.authors
        self.year.data = book.year
        self.publisher.data = book.publisher
        self.language.data = book.language_name
        self.isbn.data = book.isbn
        self.description.data = book.description
        if own:
            self.private_note.data = book.private_note


class EditBookForm(BookForm):
    submit = SubmitField("Update Book")


class AddBookForm(BookForm):
    submit = SubmitField("Add Book")


class IsbnAddForm(Form):
    """A simple form for entering ISBN of new books."""
    isbn = StringField("Quick ISBN")
    submit = SubmitField("Add")


class JsonUploadForm(Form):
    json_file = FileField("JSON file with library", validators=[
        FileRequired(),
        FileAllowed(['json'], 'JSON files only!')
    ])
    submit = SubmitField("Upload & Review")

class ChangePasswordForm(Form):
    original = PasswordField("Original password", validators=[DataRequired()])
    new = PasswordField("New password", validators=[DataRequired(), EqualTo('confirm')])
    confirm = PasswordField("Confirm new password")
    submit = SubmitField("Save")

