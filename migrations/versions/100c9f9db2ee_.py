"""empty message

Revision ID: 100c9f9db2ee
Revises: 423b3d7dc2b0
Create Date: 2014-12-26 13:49:26.428707

"""

# revision identifiers, used by Alembic.
revision = '100c9f9db2ee'
down_revision = '423b3d7dc2b0'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    op.add_column('users', sa.Column('role', sa.String(length=64), nullable=False, default="user"))


def downgrade():
    op.drop_column('users', 'role')
