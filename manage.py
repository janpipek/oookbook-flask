#!/usr/bin/env python
from flask_script import Manager, Server
from flask_script.commands import ShowUrls
from flask_migrate import MigrateCommand

from oookbook import models
from oookbook import app

manager = Manager(app)

from oookbook import database

# Turn on debugger by default and reloader
manager.add_command("runserver", Server(
    use_debugger = True,
    use_reloader = True,
    host = '0.0.0.0',
    port = 8004)
)

@manager.command
def init_db():
    database.create_all()

@manager.command
def create_user(username, email, password):
    u = models.User(username, email)
    u.password = password
    database.session.add(u)
    database.session.commit()
    print("User created.")

@manager.command
def create_admin():
    u = models.User(u"admin", u"admin@localhost")
    u.password = "admin"
    u.role = "admin"
    database.session.add(u)
    database.session.commit()
    print("User admin with password admin created.")

@manager.command
def reset_db():
    database.drop_all()
    database.create_all()

manager.add_command("show_urls", ShowUrls())

# Database migrations
manager.add_command("db", MigrateCommand)

if __name__ == "__main__":
    manager.run()
