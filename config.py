import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    PER_PAGE = 30

class DevelopmentConfig(Config):
    SECRET_KEY = "KeepThisS3cr3t"
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'postgresql://oookbook:oook@localhost/oookbook'
    BOOTSTRAP_SERVE_LOCAL = True
    UPLOAD_FOLDER = os.path.join(basedir, '_uploads')

class ProductionConfig(Config):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    BOOTSTRAP_SERVE_LOCAL = False

config = {
    "development" : DevelopmentConfig,
    "default" : DevelopmentConfig,
    "production" : ProductionConfig
}